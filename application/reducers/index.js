import {combineReducers} from 'redux';
import recorder from './recorder';
import stars from './stars';
import ball from './ball';

export default combineReducers({
  recorder,
  stars,
  ball
});
