import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {AppContainer} from 'react-hot-loader';
import {combineReducers, compose, createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import reducer from './reducers';
import sagas from './sagas';
import Application from './components';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  reducer,
  compose(
    applyMiddleware(
      sagaMiddleware,
      logger({collapsed: true})
    )
  )
);

const $node = document.getElementById('mount');

const render = Component => ReactDOM.render(
  <AppContainer>
    <Provider store={store}>
      <Component/>
    </Provider>
  </AppContainer>,
  $node
);

sagaMiddleware.run(sagas);

render(Application);

if (module.hot) {
  module.hot.accept('./components', () => {
    render(Application)
  });
}
