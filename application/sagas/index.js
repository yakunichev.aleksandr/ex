import {delay} from 'redux-saga';
import {race, take, call, put, fork, select} from 'redux-saga/effects';
import Recorder from 'recorderjs';
import * as api from 'services/api';
import * as $$recorder from 'reducers/recorder';
import * as $$stars from 'reducers/stars';
import * as $$ball from 'reducers/ball';

window.AudioContext = window.AudioContext || window.webkitAudioContext;
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
window.URL = window.URL || window.webkitURL;

export const createMediaStream = () => new Promise((resolve, reject) => navigator.getUserMedia({audio: true}, resolve, reject));

export const exportToWAV = recorder => new Promise(resolve => recorder.exportWAV(resolve));

let isEqualizerShown = false;

export function * record (recorder) {
  isEqualizerShown = true;
  recorder.record();

  const res = yield race({
    delay: call(delay, 1000 * 60 * 5),
    stop: take($$recorder.constants.SET_PREVIEW_STATE)
  });

  if (res.delay === true) {
    yield put($$recorder.actions.preview());
  }

  isEqualizerShown = false;

  recorder.stop();

  const blob = yield call(exportToWAV, recorder);

  yield put($$recorder.actions.setBlob(blob));

  const url = URL.createObjectURL(blob);

  yield put($$recorder.actions.setDownloadUrl(url));

  recorder.clear();
}

export function * watchSaveChoice() {
  while (true) {
    yield take($$recorder.constants.CHOOSE_SAVE);
    yield [
      put($$recorder.actions.prefinish()),
      put($$stars.actions.save())
    ];

    const blob = yield select($$recorder.select.blob);

    try {
      yield [
        call(delay, 1500),
        call(api.save),
        call(api.file, blob)
      ];
    } catch (err) {
      console.error(err);
    }

    yield put($$recorder.actions.finish());
  }
}

export function * watchRemoveChoice() {
  while (true) {
    yield take($$recorder.constants.CHOOSE_REMOVE);
    yield put($$ball.actions.whitify());
    yield call(delay, 1000);
    yield [
      put($$recorder.actions.prefinish()),
      put($$stars.actions.remove())
    ];

    try {
      yield [
        call(delay, 1500),
        call(api.remove)
      ]
    } catch (err) {
      console.error(err);
    }

    yield put($$recorder.actions.finish());
  }
}

export function * equalizer(analyserNode) {
  const canvas = document.getElementById('equalizer');
  const canvasWidth = canvas.width;
  const canvasHeight = canvas.height;
  const analyserContext = canvas.getContext('2d');

  function updateAnalysers() {
    if (isEqualizerShown) {
      const SPACING = 6;
      const BAR_WIDTH = 1;
      const numBars = Math.round(canvasWidth / SPACING);
      const freqByteData = new Uint8Array(analyserNode.frequencyBinCount);

      analyserNode.getByteFrequencyData(freqByteData);

      analyserContext.clearRect(0, 0, canvasWidth, canvasHeight);
      analyserContext.fillStyle = '#F6D565';
      analyserContext.lineCap = 'round';
      const multiplier = analyserNode.frequencyBinCount / numBars;

      for (let i = 0; i < numBars; ++i) {
        let magnitude = 0;
        const offset = Math.floor(i * multiplier);

        for (let j = 0; j < multiplier; j++) {
          magnitude += freqByteData[offset + j];
        }

        magnitude = magnitude / multiplier;
        const magnitude2 = freqByteData[i * multiplier];
        analyserContext.fillStyle = '#ffffff';
        analyserContext.fillRect(i * SPACING, canvasHeight, BAR_WIDTH, -magnitude);
      }

    }

    setTimeout(updateAnalysers, 1000 / 15);
    /* window.requestAnimationFrame(updateAnalysers); */
  }

  updateAnalysers();
}

export default function * () {
  const audioContext = new AudioContext();
  const stream = yield call(createMediaStream);
  const input = audioContext.createMediaStreamSource(stream);

  const inputPoint = audioContext.createGain();

  const realAudioInput = audioContext.createMediaStreamSource(stream);
  const audioInput = realAudioInput;
  audioInput.connect(inputPoint);
  const analyserNode = audioContext.createAnalyser();
  analyserNode.fftSize = 2048;
  inputPoint.connect(analyserNode);

  const recorder = new Recorder(inputPoint);

  const zeroGain = audioContext.createGain();
  zeroGain.gain.value = 0.0;
  inputPoint.connect(zeroGain);
  zeroGain.connect(audioContext.destination);

  yield fork(equalizer, analyserNode);
  yield fork(watchRemoveChoice);
  yield fork(watchSaveChoice);

  while (true) {
    yield take($$recorder.constants.SET_RECORDING_STARTED_STATE);
    yield * record(recorder);
  }
};
